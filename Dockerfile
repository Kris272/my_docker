FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y supervisor nginx php-fpm mysql-server php-mysql && apt clean

RUN sed -i 's/listen = .*/listen = 127.0.0.1:9000/' /etc/php/*/fpm/pool.d/www.conf

COPY nginx.conf /etc/supervisor/conf.d/
COPY php.conf /etc/supervisor/conf.d/
COPY mysqld.conf /etc/supervisor/conf.d/

COPY ./config_mysql.sh /config_mysql.sh
RUN bash config_mysql.sh

COPY default /etc/nginx/sites-available/
COPY index.php /var/www/html/
COPY insert.php /var/www/html/
COPY select.php /var/www/html/
COPY info.php /var/www/html/
WORKDIR /data
VOLUME /data
EXPOSE 80

CMD ["supervisord", "-n"]
